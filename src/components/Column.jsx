export const COLUMNS = [
    {
        header: 'Id',
        accessor:'id'
    },
    {
        header: 'Account_type',
        accessor:'account_type'
    },
    {
        header: 'Balance',
        accessor:'balance'
    },
    {
        header: 'Exchange',
        accessor:'exchange'
    },
    {
        header: 'Subaccount',
        accessor:'subaccount'
    },
    {
        header: 'T_create',
        accessor:'t_create'
    },
    {
        header: 'T_update',
        accessor:'t_update'
    },
    {
        header: 'Token',
        accessor:'token'
    },
]

