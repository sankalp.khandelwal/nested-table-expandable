import React, { useMemo } from 'react'
import { useTable } from 'react-table';
// import balances_summary from './BalanceSummary.json';
import balances_summary from './BalanceSummary.json';
import { COLUMNS } from './Column';
import './BasicTable.css';


export const BasicTable = () => {

    const columns = useMemo(() => COLUMNS, [])
    const data = useMemo(() => balances_summary, [])


    const tableinstance = useTable({
        columns,
        data
    })

    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = tableinstance;
    return (
        <table {...getTableProps()}>
            <thead >
                {headerGroups.map((headerGroups) => (
                    <tr {...headerGroups.getFooterGroupProps()}>
                        {
                            headerGroups.headers.map((column) => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('header')}
                                </th>
                            ))
                        }
                    </tr>
                ))}
            </thead>
            <tbody {...getTableBodyProps()}>
                {rows.map(row => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}>
                            {
                                row.cells.map(cell => {
                                    return <td {...cell.getCellProps()}>
                                        {
                                            cell.render('Cell')
                                        }
                                    </td>
                                })
                            }
                        </tr>
                    )
                })}

            </tbody>
        </table>
    )
}
